import React, { useState } from 'react';
import {Button,Form,FormGroup,Label,Input} from  'reactstrap'
import { FacebookLoginButton } from "react-social-login-buttons";

function Login(props) {
  const username = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const handleLogin = () => {
    props.history.push('/');
  }

        return (
            <Form className="login-form">
                 <h1>Login Page</h1>
                <FormGroup>
                <div className = "form-group">
                    <label> Login </label>
                    <input placeholder="login" {...username} className="form-control"/>
                </div>
                <div className = "form-group">
                <label> Password </label>
                 <input type="password" {...password} className="form-control" />
                </div>
                    <button className="btn lg btn-dark btn-block" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading}>Login</button>
                <div className="text-center pt-3"> Or continue with your social account</div>
                <FacebookLoginButton className="mt-3 mb-3"/>
                <a href="/sign-up">Sign-up</a>
                <span className="p-2">|</span>
                <a href="/forgot-password">Forgot password</a>
                </FormGroup>
            </Form>
        )
    
}

const useFormInput = initialValue => {
    const [value, setValue] = useState(initialValue);
  
    const handleChange = e => {
      setValue(e.target.value);
    }
    return {
      value,
      onChange: handleChange
    }
  }
export default Login
